const express = require("express");

const router = express.Router();

router.use("/UserData", require("./userData"));

module.exports = router;
