var express = require('express')
var router = express.Router()
var AWS = require('aws-sdk');


function isReqAuthorized(req) {
  var header=req.headers['authorization']||'',        // get the header
  token=header.split(/\s+/).pop()||'',            // and the encoded auth token
  auth=new Buffer.from(token, 'base64').toString(),    // convert from base64
  parts=auth.split(/:/),                          // split on colon
  username=parts[0],
  password=parts[1];
  if(username == "twinpass" && password == "Pa$$w0rd") {
    return true;
  }
  return false;
}

router.get('/TestGet', async function (req, res) {
  var isAuthorized = isReqAuthorized(req);
  if(!isAuthorized) {
    res.send({
      statusCode: 401,
      body: null,
      headers: {"content-type": "application/json"}
    });
    return;
  } 
  
  res.send( {
    statusCode: 200,
    body: JSON.stringify({"username": "username",
  "password": "password"}),
    headers: {"content-type": "application/json"}
});
});

router.post('/TestPost', async function (req, res) { 
  var isAuthorized = isReqAuthorized(req);
  if(!isAuthorized) {
    res.send({
      statusCode: 401,
      body: null,
      headers: {"content-type": "application/json"}
    });
    return;
  } 
  var body = req.body;
  var facebookId = req.body.facebookId;
  var data = req.body.data;
  res.send({"facebookId": facebookId, "data": data});
});

router.get('/GetUserData', async function (req, res) {
  var isAuthorized = isReqAuthorized(req);
  if(!isAuthorized) {
    res.send({
      statusCode: 401,
      body: null,
      headers: {"content-type": "application/json"}
    });
    return;
  } 

  AWS.config.update({
    region: "us-west-2"
  });
  
 
  var docClient = new AWS.DynamoDB.DocumentClient();
  var table = "testtabletwinpass";
  var facebookId = req.query.facebookId;
  
  var params = {
      TableName:table,
      Key:{
          "facebookId": facebookId
      }
  };
  var response = await docClient.get(params)
  .promise()
  .then(data => {
    
    if(data.Item == null) {
      return {
        statusCode: 200,
        body: null,
        headers: {"content-type": "application/json"}
    };
    }
    return {
      statusCode: 200,
      body: data.Item.data,
      headers: {"content-type": "application/json"}
  };
  })
  .catch(err => {
    return {
      statusCode: 400,
      body: JSON.stringify(err),
      headers: {"content-type": "application/json"}
  };
  });
  res.send(response);
});

router.post('/UpdateUserData', async function (req, res) {
  var isAuthorized = isReqAuthorized(req);
  if(!isAuthorized) {
    res.send({
      statusCode: 401,
      body: null,
      headers: {"content-type": "application/json"}
    });
    return;
  }
  AWS.config.update({
    region: "us-west-2"
  });
  
  var docClient = new AWS.DynamoDB.DocumentClient();
  
  var table = "testtabletwinpass";
  var facebookId = req.body.facebookId;
  var data = req.body.data;
  var params = {
      TableName:table,
      Item:{
          "facebookId": facebookId,
          "data": data
      }
  };
  
  console.log("Adding a new item...");
  var response = await docClient.put(params)
  .promise()
  .then(data => {
    return {
      statusCode: 200,
      body: "Item updated successfully",
      headers: {"content-type": "application/json"}
  };
  })
  .catch(err => {
    return {
      statusCode: 400,
      body: "Item not updated",
      headers: {"content-type": "application/json"}
  };
  });
  res.send(response);
});

module.exports = router;